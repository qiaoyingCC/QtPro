#include "widget.h"
#include "ui_widget.h"

#include <QSqlRecord>
#include <QSqlQuery>
#include <QDebug>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    m_SearchCond = Search_None;

    m_OperCond = Oper_None;

    on_pb_search_clicked();
}

Widget::~Widget()
{
    delete ui;
}

void Widget::updataTableDates(void)
{
    ui->tableWidget->setColumnCount(4);  //设置列数
    ui->tableWidget->setRowCount(m_InfoList.length());  //设置行数

    // set header lables(设置第一列的头名称)
    QStringList headers;
    headers << "学号" << "姓名" << "年级" << "专业";
    ui->tableWidget->setHorizontalHeaderLabels(headers);
    for(int i=0; i < m_InfoList.count(); i++)
    {
        QTableWidgetItem *item = new QTableWidgetItem(m_InfoList.at(i).getID());
        ui->tableWidget->setItem(i, 0, item);
        item = new QTableWidgetItem(m_InfoList.at(i).getName());
        ui->tableWidget->setItem(i, 1, item);
        item = new QTableWidgetItem(m_InfoList.at(i).getDegree());
        ui->tableWidget->setItem(i, 2, item);
        item = new QTableWidgetItem(m_InfoList.at(i).getMajor());
        ui->tableWidget->setItem(i, 3, item);
    }
}

void Widget::SelectStudentInfos(QString queryString)
{
    m_InfoList.clear();
//    qDebug() << queryString;
    QSqlQuery query;
    if(query.exec(queryString))
    {
        int id_idx = query.record().indexOf("id");
        int name_idx = query.record().indexOf("name");
        int degree_idx = query.record().indexOf("degree");
        int major_idx = query.record().indexOf("major");
        while (query.next())
        {
            QString id = query.value(id_idx).toString();
            QString name = query.value(name_idx).toString();
            QString degree = query.value(degree_idx).toString();
            QString major = query.value(major_idx).toString();

            StudentInfo info(id, name, degree, major);
            m_InfoList.append(info);
        }
    }
    updataTableDates();
}

void Widget::on_cb_condition_currentIndexChanged(int index)
{
    m_SearchCond = index;
    if(m_SearchCond == Search_None)
    {
        ui->le_condition->setEnabled(false);
    }else
    {
        ui->le_condition->setEnabled(true);
    }
}

void Widget::on_pb_search_clicked()
{
    QString queryString;

    if(m_SearchCond == Search_None)
    {
        queryString = QString("select * from student_info");
    }else if(m_SearchCond == Search_ID)
    {
        queryString = QString("select * from student_info "
                              "where id = '%1'").arg(ui->le_condition->text());
    }else if(m_SearchCond == Search_Name)
    {
        queryString = QString("select * from student_info "
                              "where Name = '%1'").arg(ui->le_condition->text());
    }else if(m_SearchCond == Search_Degree)
    {
        queryString = QString("select * from student_info "
                              "where Degree = '%1'").arg(ui->le_condition->text());
    }else if(m_SearchCond == Search_Major)
    {
        queryString = QString("select * from student_info "
                              "where Major = '%1'").arg(ui->le_condition->text());
    }
//    qDebug() << queryString;
    SelectStudentInfos(queryString);
}

void Widget::on_tableWidget_clicked(const QModelIndex &index)
{
    StudentInfo info = m_InfoList.at(index.row());
    ui->le_id->setText(info.getID());
    ui->le_name->setText(info.getName());
    ui->le_degree->setText(info.getDegree());
    ui->le_major->setText(info.getMajor());
}

void Widget::on_pb_modify_clicked()
{
    m_OperCond = Oper_Mod;
    ui->le_name->setEnabled(true);
    ui->le_degree->setEnabled(true);
    ui->le_major->setEnabled(true);
}

void Widget::on_pb_delete_clicked()
{
    m_OperCond = Oper_Del;
    ui->le_name->setEnabled(true);
    ui->le_degree->setEnabled(true);
    ui->le_major->setEnabled(true);
}

void Widget::on_pb_add_clicked()
{
    m_OperCond = Oper_Add;
    ui->le_id->setEnabled(true);
    ui->le_name->setEnabled(true);
    ui->le_degree->setEnabled(true);
    ui->le_major->setEnabled(true);
}

void Widget::on_pb_cancel_clicked()
{
    m_OperCond = Oper_None;
    ui->le_id->setEnabled(false);
    ui->le_name->setEnabled(false);
    ui->le_degree->setEnabled(false);
    ui->le_major->setEnabled(false);
}

void Widget::on_pb_save_clicked()
{
    QString queryString;
    QSqlQuery query;
    if(m_OperCond == Oper_Mod)
    {
        queryString = QString("update student_info set name = '%1', degree = '%2', "
                              "major = '%3' where id = '%4'").arg(ui->le_name->text())
                              .arg(ui->le_degree->text()).arg(ui->le_major->text()).
                              arg(ui->le_id->text());
    }else if(m_OperCond == Oper_Del)
    {
        queryString = QString("delete from student_info where id = '%1'").
                              arg(ui->le_id->text());
    }else if(m_OperCond == Oper_Add)
    {
       queryString = QString("insert into student_info values('%1', '%2', '%3','%4')")
                              .arg(ui->le_id->text()).arg(ui->le_name->text()).
                              arg(ui->le_degree->text()).arg(ui->le_major->text());
    }
    qDebug() << queryString << ": " << query.exec(queryString);

    on_pb_search_clicked();
}

