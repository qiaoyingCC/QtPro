#include "widget.h"
#include <QApplication>

#include "connectmysql.h"
#include "connectsqlite.h"

#include <QDebug>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    ConnectSqlite sql;
    if (sql.createConnection())
    {
        qDebug() << "Connect Success!";
    }

    Widget w;
    w.show();

    int rec = a.exec();
    sql.closeConnection();

    return rec;
}
