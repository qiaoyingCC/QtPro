#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

#include "studentinfo.h"

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

enum SearchCond{
    Search_None = 0,
    Search_ID,
    Search_Name,
    Search_Degree,
    Search_Major
};

enum Oper_Cond{
    Oper_None = 0,
    Oper_Mod,
    Oper_Del,
    Oper_Add,
};

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();


    void updataTableDates(void);
    void SelectStudentInfos(QString queryString);

private slots:
    void on_cb_condition_currentIndexChanged(int index);

    void on_pb_search_clicked();

    void on_tableWidget_clicked(const QModelIndex &index);

    void on_pb_modify_clicked();

    void on_pb_delete_clicked();

    void on_pb_add_clicked();

    void on_pb_cancel_clicked();

    void on_pb_save_clicked();


private:
    Ui::Widget *ui;

    StudentInfoList m_InfoList;
    int m_SearchCond;
    int m_OperCond;

};

#endif // WIDGET_H
