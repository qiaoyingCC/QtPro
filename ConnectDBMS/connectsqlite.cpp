#include "connectsqlite.h"

#include <QsqlDatabase>
#include <QSqlError>

ConnectSqlite::ConnectSqlite()
{

}

bool ConnectSqlite::createConnection()
{
    QSqlDatabase db;
    db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("../ConnectDBMS/myedu");
    if(!db.open())
    {
        qCritical("Can't open database: %s(%s)",
        db.lastError().text().toLocal8Bit().data(),
        qt_error_string().toLocal8Bit().data());
        return false;
    }
    return true;
}

void ConnectSqlite::closeConnection()
{
    QSqlDatabase::database().close();
}

