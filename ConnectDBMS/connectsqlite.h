#ifndef CONNECTSQLITE_H
#define CONNECTSQLITE_H


class ConnectSqlite
{
public:
    ConnectSqlite();

    bool createConnection();
    void closeConnection();
};

#endif // CONNECTSQLITE_H
