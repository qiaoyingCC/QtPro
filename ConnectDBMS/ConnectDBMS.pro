#-------------------------------------------------
#
# Project created by QtCreator 2019-05-25T09:41:34
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ConnectDBMS
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    connectmysql.cpp \
    connectsqlite.cpp \
    studentinfo.cpp

HEADERS  += widget.h \
    connectmysql.h \
    connectsqlite.h \
    studentinfo.h

FORMS    += widget.ui
