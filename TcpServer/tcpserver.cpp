#include "tcpserver.h"
#include "ui_tcpserver.h"

#include <QTime>
#include <QDebug>

TcpServer::TcpServer(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TcpServer)
{
    ui->setupUi(this);


    m_Server = new QTcpServer(this);
    connect(m_Server, SIGNAL(newConnection()),
            this, SLOT(serverConnected()));
    m_Server->listen(QHostAddress::Any, 55555);
}

TcpServer::~TcpServer()
{
    delete ui;
}

void TcpServer::serverConnected()
{
//    qDebug() << "serverConnected(): " << QTime::currentTime();
    QTcpSocket *connection = m_Server->nextPendingConnection();
    connect(connection, SIGNAL(disconnected()),
    connection, SLOT(deleteLater()));

    QByteArray buffer;
    QDataStream out(&buffer, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_6);
    QString greeting = QString("Hello! The time is %1")
                       .arg(QTime::currentTime().toString());
    ui->listWidget->addItem(greeting);
    out << (quint16)0;
    out << greeting;
    out.device()->seek(0);
    out << (quint16)(buffer.size() - sizeof(quint16));

    connection->write(buffer);
    connection->disconnectFromHost();

}
