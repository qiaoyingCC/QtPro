#-------------------------------------------------
#
# Project created by QtCreator 2019-05-15T23:02:14
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TeacherMS
TEMPLATE = app


SOURCES += main.cpp\
        teacherms.cpp \
    teacherinfo.cpp

HEADERS  += teacherms.h \
    teacherinfo.h

FORMS    += teacherms.ui
