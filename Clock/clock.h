#ifndef CLOCK_H
#define CLOCK_H

#include <QWidget>
#include <QDebug>
#include <QPaintEvent>

namespace Ui {
class Clock;
}

class Clock : public QWidget
{
    Q_OBJECT

public:
    explicit Clock(QWidget *parent = 0);
    ~Clock();

private:
    Ui::Clock *ui;

    int m_Hours;
    int m_minutes;
    int m_second;

protected:
    void paintEvent(QPaintEvent *);

public slots:
    void slotTimeout();
};

#endif // CLOCK_H
