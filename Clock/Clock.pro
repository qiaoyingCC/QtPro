#-------------------------------------------------
#
# Project created by QtCreator 2019-05-04T14:51:49
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Clock
TEMPLATE = app


SOURCES += main.cpp\
        clock.cpp

HEADERS  += clock.h

FORMS    += clock.ui
