#include "clock.h"
#include "ui_clock.h"
#include <QPainter>
#include <QTimer>
#include <QTime>
#include <QDebug>

Clock::Clock(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Clock)
{
    ui->setupUi(this);

//    QTime t = QTime::currentTime();
    m_Hours = QTime::currentTime().hour();
    m_minutes = QTime::currentTime().minute();
    m_second = QTime::currentTime().second();
    qDebug() << m_Hours << ", " << m_minutes << ", " << m_second;


    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()),
            this, SLOT(slotTimeout()));
    timer->start(1000);
}

Clock::~Clock()
{
    delete ui;
}

void Clock::slotTimeout()
{
//    qDebug() << "slotTimeout: ";


    m_second++;
    if(m_second % 60 == 0)
    {
        m_minutes++;
        if(m_minutes % 60 == 0)
        {
            m_Hours++;
        }
    }
    repaint();
}

void Clock::paintEvent(QPaintEvent *)
{
//    qDebug() << "paintEvent" ;
//    QPainter painter(this);
//    QPen pen(Qt::green);
//    QBrush brush(QColor(0, 150, 0, 150), Qt::Dense1Pattern);
//    painter.setBrush(brush);
//    painter.setPen(pen);
//    painter.drawEllipse(QPoint(100, 100), 80, 60);
//    painter.drawEllipse(100, 100, 200, 200);
//    //painter.setBrush(brush);
//    painter.setBrush(Qt::NoBrush);
//    painter.drawRect(100, 100, 200, 200);

//    QFont font("草书");
//    font.setPixelSize(50);
//    painter.setFont(font);
//    painter.drawText(this->rect(), 0, "hello world");


    QPainter p(this);
    int extent;
    if (width()>height())
    extent = height()-20;
    else
    extent = width()-20;
    p.translate((width()-extent)/2, (height()-extent)/2);
    QPen pen(QColor(0, 153, 204, 150), 4);
    p.setPen(pen);
    QBrush brush(QColor(255, 153, 204, 150), Qt::Dense1Pattern);
    p.setBrush(brush);
    //椭圆
    p.drawEllipse(0, 0, extent, extent);

    p.translate(extent/2, extent/2);  //移动坐标系
    for(int angle=0; angle<=360; angle+=6)
    {
        p.save();
        if(angle == 0 || angle == 90 || angle == 180 || angle == 270
           || angle == 360)
        {
            p.rotate(angle); //顺时针旋转angle度
            p.drawLine(extent*0.44, 0, extent*0.5, 0);//画线，从(0.42, 0)点到(0.48, 0)
        }
        else if(angle == 30 || angle == 60 || angle == 120 || angle == 150 ||
                angle == 210 || angle == 240 || angle == 300 || angle == 330)
        {
            QPen angle12_pen(QColor(0, 153, 204, 150), 3);
            p.setPen(angle12_pen);
            p.rotate(angle);
            p.drawLine(extent*0.45, 0, extent*0.5, 0);
        }else
        {
            QPen angle60_pen(QColor(0, 153, 204, 150), 2);
            p.setPen(angle60_pen);
            p.rotate(angle);
            p.drawLine(extent*0.48, 0, extent*0.5, 0);
        }
        p.restore();
    }


//------------------------------绘制时针---------------------------------
    p.save();
    p.rotate(m_Hours * 30 + m_minutes * 0.5 - 90);

    //-----------绘制时针为三角形----------------
//    QPolygon Hours;
//    Hours << QPoint(-extent*0.025, extent*0.025)
//            << QPoint(-extent*0.025, -extent*0.025)
//            << QPoint(extent*0.26, 0);
//    p.setPen(Qt::NoPen);
//    p.setBrush(QColor(255,0,0,150));
//    p.drawPolygon(Hours);

    QPen Hours_pen(Qt::black, 3);
    p.setPen(Hours_pen);
    p.drawLine(QPoint(-extent*0.008, extent*0.008), QPoint(extent*0.3, 0));
    p.restore();


//------------------------------绘制分针---------------------------------
    p.save();
    p.rotate(m_minutes * 6 - 90);

    //-----------绘制分针为三角形----------------
//    QPolygon minutes;
//    minutes << QPoint(-extent*0.025, extent*0.025)
//            << QPoint(-extent*0.025, -extent*0.025)
//            << QPoint(extent*0.36, 0);
//    p.setPen(Qt::NoPen);
//    p.setBrush(QColor(255,10,0,120));
//    p.drawPolygon(minutes);


    QPen minutes_pen(Qt::black, 2);
    p.setPen(minutes_pen);
    p.drawLine(QPoint(-extent*0.008, extent*0.008), QPoint(extent*0.46, 0));
    p.restore();

//------------------------------绘制秒针---------------------------------
    p.save();
    p.rotate(m_second * 6-90);

    //-----------绘制秒针为三角形----------------
//    QPolygon seconds;
//    seconds<< QPoint(-extent*0.025, extent*0.025)
//            << QPoint(-extent*0.025, -extent*0.025)
//            << QPoint(extent*0.46, 0);
//    p.setBrush(QColor(255,0,0,120));
//    p.drawPolygon(seconds);


    QPen second_pen(Qt::black, 1);
    p.setPen(second_pen);
    p.drawLine(QPoint(-extent*0.008, extent*0.008), QPoint(extent*0.47, 0));
    p.restore();
}
