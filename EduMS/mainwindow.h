#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "studentms.h"
#include "teacherms.h"
#include "classms.h"
#include "coursems.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_actionStudentMS_triggered();

    void on_actionTeacherMS_triggered();

    void on_actionClassMS_triggered();

    void on_actionCourseMS_triggered();

private:
    Ui::MainWindow *ui;

    StudentMS *m_studentMS;
    TeacherMS *m_teacherMS;
    ClassMS *m_classMS;
    CourseMs *m_courseMS;
};

#endif // MAINWINDOW_H
