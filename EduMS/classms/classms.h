#ifndef CLASSMS_H
#define CLASSMS_H

#include <QWidget>

#include "classinfo.h"

namespace Ui {
class ClassMS;
}

class ClassMS : public QWidget
{
    Q_OBJECT

public:
    explicit ClassMS(QWidget *parent = 0);
    ~ClassMS();

    void readFileInfo();
    void updataTableInfos();
    void saveFileInfo();

    friend bool operator ==(const ClassInfo &info1, const ClassInfo &info2);

    enum Search_Condition{
        Search_None,
        Search_Classid,
        Search_Definition,
        Search_Classroom,
        Search_Number
    };

    enum oper_Data{
        Oper_None,
        Oper_Mdy,
        Oper_Del,
        Oper_Add,
    };

private slots:
    void on_cb_condition_currentIndexChanged(int index);
    void on_pb_search_clicked();
    void on_tableWidget_clicked(const QModelIndex &index);
    void on_pb_modify_clicked();
    void on_pb_delete_clicked();
    void on_pb_add_clicked();
    void on_pb_cancel_clicked();
    void on_pb_save_clicked();

private:
    Ui::ClassMS *ui;

    ClassInfoList m_infoList;
    ClassInfoPtrList m_infoPtrList;
    int m_searchCond;
    int m_currentRow;
    int m_operData;
    ClassInfo * m_operInfo;

};

#endif // CLASSMS_H
