INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

FORMS += \
    $$PWD/classms.ui

HEADERS += \
    $$PWD/classinfo.h \
    $$PWD/classms.h

SOURCES += \
    $$PWD/classinfo.cpp \
    $$PWD/classms.cpp