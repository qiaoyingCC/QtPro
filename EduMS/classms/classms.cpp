#include "classms.h"
#include "ui_classms.h"

#include <QFile>
#include <QTextStream>
#include <QDebug>

ClassMS::ClassMS(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ClassMS)
{
    ui->setupUi(this);

    m_searchCond = Search_None;
    m_operData = Oper_None;

    ui->le_condition->setEnabled(false);

    readFileInfo();


}

ClassMS::~ClassMS()
{
    saveFileInfo();

    delete ui;
}

void ClassMS::readFileInfo()
{
    m_infoList.clear();
    QFile f("../ClassMS/data/class.txt");
    if(f.open(QIODevice::ReadOnly))
    {
        QTextStream t(&f);
        QString line;
        QStringList list;
        while(!t.atEnd())
        {
            line.clear();
            list.clear();
            line = t.readLine();
            list = line.split("\t\t");
            ClassInfo info(list.at(0), list.at(1), list.at(2), list.at(3));
            m_infoList.append(info);
        }
        f.close();
    }
}

void ClassMS::updataTableInfos()
{
    ui->tableWidget->clear();
    ui->tableWidget->setColumnCount(4);
    ui->tableWidget->setRowCount(m_infoPtrList.length());

    QStringList headers;
    headers << "班级号" << "名称" << "教室" << "人数";
    ui->tableWidget->setHorizontalHeaderLabels(headers);
    for(int i = 0; i < m_infoPtrList.count(); i++)
    {
        QTableWidgetItem *item = new QTableWidgetItem(m_infoList.at(i).getClassid());
        ui->tableWidget->setItem(i, 0, item);
        item = new QTableWidgetItem(m_infoList.at(i).getDefinition());
        ui->tableWidget->setItem(i, 1, item);
        item = new QTableWidgetItem(m_infoList.at(i).getClassroom());
        ui->tableWidget->setItem(i, 2, item);
        item = new QTableWidgetItem(m_infoList.at(i).getNumber());
        ui->tableWidget->setItem(i, 3, item);
    }
}

void ClassMS::saveFileInfo()
{
    QFile f("../ClassMS/data/class.txt");
    if(f.open(QIODevice::WriteOnly | QIODevice::Truncate))
    {
        QTextStream t(&f);
        foreach(const ClassInfo &info, m_infoList)
        {
            t << info.getClassid() << "\t\t" << info.getDefinition() << "\t\t"
              << info.getClassroom() << "\t\t" << info.getNumber() << "\n";
        }
        f.close();
    }
}

void ClassMS::on_cb_condition_currentIndexChanged(int index)
{
    m_searchCond = index;
    if(m_searchCond == 0)
    {
        ui->le_condition->setEnabled(false);
    }else
    {
        ui->le_condition->setEnabled(true);
    }
}

void ClassMS::on_pb_search_clicked()
{
    m_infoPtrList.clear();
    if(m_searchCond == Search_Classid)
    {
        for(int i = 0; i < m_infoList.length(); i++)
        {
            if(ui->le_condition->text() == m_infoList.at(i).getClassid())
            {
                m_infoPtrList.append(&(m_infoList[i]));
            }
        }
    }else if(m_searchCond == Search_Definition)
    {
        for(int i = 0; i < m_infoList.length(); i++)
        {
            if(ui->le_condition->text() == m_infoList.at(i).getDefinition())
            {
                m_infoPtrList.append(&(m_infoList[i]));
            }
        }
    }else if(m_searchCond == Search_Classroom)
    {
        for(int i = 0; i < m_infoList.length(); i ++)
        {
            if(ui->le_condition->text() == m_infoList.at(i).getClassroom())
            {
                m_infoPtrList.append(&(m_infoList[i]));
            }
        }
    }else if(m_searchCond == Search_Number)
    {
        for(int i = 0; i < m_infoList.length(); i++)
        {
            if(ui->le_condition->text() == m_infoList.at(i).getNumber())
            {
                m_infoPtrList.append(&(m_infoList[i]));
            }
        }
    }else
    {
        for(int i = 0; i < m_infoList.length(); i++)
        {
            m_infoPtrList.append(&(m_infoList[i]));
        }
    }
    updataTableInfos();
}

void ClassMS::on_tableWidget_clicked(const QModelIndex &index)
{
    m_currentRow = index.row();
    m_operInfo = m_infoPtrList.at(m_currentRow);
    ui->le_classid->setText(m_operInfo->getClassid());
    ui->le_definition->setText(m_operInfo->getDefinition());
    ui->le_classroom->setText(m_operInfo->getClassroom());
    ui->le_number->setText(m_operInfo->getNumber());
}

void ClassMS::on_pb_modify_clicked()
{
    m_operData = Oper_Mdy;
    ui->le_definition->setEnabled(true);
    ui->le_classroom->setEnabled(true);
    ui->le_number->setEnabled(true);
}

void ClassMS::on_pb_delete_clicked()
{
    m_operData = Oper_Del;
    ui->le_classid->setEnabled(true);
    ui->le_definition->setEnabled(true);
    ui->le_classroom->setEnabled(true);
    ui->le_number->setEnabled(true);
}

void ClassMS::on_pb_add_clicked()
{
    m_operData = Oper_Add;
    ui->le_classid->setEnabled(true);
    ui->le_definition->setEnabled(true);
    ui->le_classroom->setEnabled(true);
    ui->le_number->setEnabled(true);
}

void ClassMS::on_pb_cancel_clicked()
{
    m_operData = Oper_None;
    ui->le_classid->setEnabled(false);
    ui->le_definition->setEnabled(false);
    ui->le_classroom->setEnabled(false);
    ui->le_number->setEnabled(false);
}

void ClassMS::on_pb_save_clicked()
{
    if(m_operData == Oper_Mdy)
    {
        m_operInfo->setClassid(ui->le_classid->text());
        m_operInfo->setDefinition(ui->le_definition->text());
        m_operInfo->setClassroom(ui->le_classroom->text());
        m_operInfo->setNumber(ui->le_number->text());
    }else if(m_operData == Oper_Del)
    {
        m_infoList.removeOne(*m_operInfo);
    }else if(m_operData == Oper_Add)
    {
        ClassInfo info(ui->le_classid->text(), ui->le_definition->text(),
                       ui->le_classroom->text(), ui->le_number->text());
        m_infoList.append(info);
    }
    on_pb_search_clicked();
    saveFileInfo();
}

bool operator ==(const ClassInfo &info1, const ClassInfo &info2)
{
    return info1.getClassid() == info2.getClassid();
}
