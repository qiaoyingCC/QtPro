#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    this->setCentralWidget(ui->widget);
    this->statusBar()->hide();
    this->menuBar()->hide();

    m_studentMS = new StudentMS(ui->widget);
    m_teacherMS = new TeacherMS(ui->widget);
    m_classMS = new ClassMS(ui->widget);
    m_courseMS = new CourseMs(ui->widget);

    on_actionStudentMS_triggered();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionStudentMS_triggered()
{
    m_studentMS->show();
    m_teacherMS->hide();
    m_classMS->hide();
    m_courseMS->hide();
}

void MainWindow::on_actionTeacherMS_triggered()
{
    m_studentMS->hide();
    m_teacherMS->show();
    m_classMS->hide();
    m_courseMS->hide();
}

void MainWindow::on_actionClassMS_triggered()
{
    m_studentMS->hide();
    m_teacherMS->hide();
    m_classMS->show();
    m_courseMS->hide();
}

void MainWindow::on_actionCourseMS_triggered()
{
    m_studentMS->hide();
    m_teacherMS->hide();
    m_classMS->hide();
    m_courseMS->show();
}
