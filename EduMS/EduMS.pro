#-------------------------------------------------
#
# Project created by QtCreator 2019-05-18T10:00:40
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = EduMS
TEMPLATE = app

INCLUDEPATH += $$PWD/include
include(studentms/student.pri)
include(teacherms/teacher.pri)
include(coursems/course.pri)
include(classms/class.pri)


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += \
    mainwindow.h

FORMS    += \
    mainwindow.ui

RESOURCES += \
    icons/icons.qrc

DISTFILES += \
    studentms/StudentMS.pri
