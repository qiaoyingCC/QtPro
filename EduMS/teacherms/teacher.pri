INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

FORMS += \
    $$PWD/teacherms.ui

HEADERS += \
    $$PWD/teacherinfo.h \
    $$PWD/teacherms.h

SOURCES += \
    $$PWD/teacherinfo.cpp \
    $$PWD/teacherms.cpp