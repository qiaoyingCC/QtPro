#include "teacherinfo.h"

#include <QDebug>

TeacherInfo::TeacherInfo()
{
    m_jobnum.clear();
    m_name.clear();
    m_department.clear();
    m_job.clear();
}

TeacherInfo::TeacherInfo(const QString &jobnum, const QString &name,
                         const QString &department, const QString &job)
{
    m_jobnum = jobnum;
    m_name = name;
    m_department = department;
    m_job = job;
}

TeacherInfo::~TeacherInfo()
{

}

void TeacherInfo::display(void) const
{
    qDebug() << "Jobnum: " << m_jobnum;
    qDebug() << "Name: " << m_name;
    qDebug() << "Department: " << m_department;
    qDebug() << "Job: " << m_job;
}

void TeacherInfo::setJobnum(const QString &jobnum)
{
    m_jobnum = jobnum;
}

const QString &TeacherInfo::getJobnum() const
{
    return m_jobnum;
}

void TeacherInfo::setName(const QString &name)
{
    m_name = name;
}

const QString &TeacherInfo::getName() const
{
    return m_name;
}

void TeacherInfo::setDepartment(const QString &department)
{
    m_department = department;
}

const QString &TeacherInfo::getDepartment() const
{
    return m_department;
}

void TeacherInfo::setJob(const QString &job)
{
    m_job = job;
}

const QString & TeacherInfo::getJob() const
{
    return m_job;
}
