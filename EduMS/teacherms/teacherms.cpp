#include "teacherms.h"
#include "ui_teacherms.h"

#include <QFile>
#include <QTextStream>
#include <QDebug>

TeacherMS::TeacherMS(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TeacherMS)
{
    ui->setupUi(this);

    readFileInfo();

    m_SearchCond = Search_None;
    m_operData = Oper_None;
    ui->le_condition->setEnabled(false);
}

TeacherMS::~TeacherMS()
{
    saveFileInfo();

    delete ui;
}

void TeacherMS::readFileInfo()
{
    m_infoList.clear();
    QFile f("../TeacherMS/data/teacher.txt");
    if(f.open(QIODevice::ReadOnly))
    {
        QTextStream t(&f);
        QString line;
        QStringList list;
        while(!t.atEnd())
        {
            line.clear();
            list.clear();
            line = t.readLine();
            list = line.split("\t\t");
            TeacherInfo info(list.at(0), list.at(1), list.at(2), list.at(3));
            m_infoList.append(info);
//            qDebug() << list;
        }
        f.close();
    }
}

void TeacherMS::updataTableInfos()
{
    ui->tableWidget->clear();
    ui->tableWidget->setColumnCount(4);
    ui->tableWidget->setRowCount(m_infoPtrList.length());

    QStringList headers;
    headers << "工号" << "姓名" << "部门" << "职务";
    ui->tableWidget->setHorizontalHeaderLabels(headers);
    for(int i = 0; i < m_infoPtrList.count(); i++)
    {
        QTableWidgetItem *item = new QTableWidgetItem(m_infoPtrList.at(i)->getJobnum());
        ui->tableWidget->setItem(i, 0, item);
        item = new QTableWidgetItem(m_infoPtrList.at(i)->getName());
        ui->tableWidget->setItem(i, 1, item);
        item = new QTableWidgetItem(m_infoPtrList.at(i)->getDepartment());
        ui->tableWidget->setItem(i, 2, item);
        item = new QTableWidgetItem(m_infoPtrList.at(i)->getJob());
        ui->tableWidget->setItem(i, 3, item);
    }
}

void TeacherMS::saveFileInfo()
{
    QFile f("../TeacherMS/data/teacher.txt");
    if(f.open(QIODevice::WriteOnly | QIODevice::Truncate))
    {
        QTextStream t(&f);
        foreach(const TeacherInfo &info, m_infoList)
        {
            t << info.getJobnum() << "\t\t" << info.getName() << "\t\t"
              << info.getDepartment() << "\t\t" << info.getJob() << "\n";
        }
        f.close();
    }
}

void TeacherMS::on_cb_condition_currentIndexChanged(int index)
{
    m_SearchCond = index;
    if(m_SearchCond == 0)
    {
        ui->le_condition->setEnabled(false);
    }else
    {
        ui->le_condition->setEnabled(true);
    }
}

void TeacherMS::on_pb_search_clicked()
{
    m_infoPtrList.clear();
    if(m_SearchCond == Search_Jobnum)
    {
        for(int i = 0; i < m_infoList.length(); i++)
        {
            if(ui->le_condition->text() == m_infoList.at(i).getJobnum())
            {
                m_infoPtrList.append(&(m_infoList[i]));
            }
        }
    }else if(m_SearchCond == Search_Name)
    {
        for(int i = 0; i < m_infoList.length(); i++)
        {
            if(ui->le_condition->text() == m_infoList.at(i).getName())
            {
                m_infoPtrList.append(&(m_infoList[i]));
            }
        }
    }else if(m_SearchCond == Search_Department)
    {
        for(int i = 0; i < m_infoList.length(); i++)
        {
            if(ui->le_condition->text() == m_infoList.at(i).getDepartment())
            {
                m_infoPtrList.append(&(m_infoList[i]));
            }
        }
    }else if(m_SearchCond == Search_Job)
    {
        for(int i = 0; i < m_infoList.length(); i++)
        {
            if(ui->le_condition->text() == m_infoList.at(i).getJob())
            {
                m_infoPtrList.append(&(m_infoList[i]));
            }
        }
    }else
    {
        for(int i = 0; i < m_infoList.length(); i++)
        {
            m_infoPtrList.append(&(m_infoList[i]));
        }
    }
    updataTableInfos();
}

void TeacherMS::on_tableWidget_clicked(const QModelIndex &index)
{
    m_currentRow = index.row();
    m_operInfo = m_infoPtrList.at(m_currentRow);
    ui->le_jobnum->setText(m_operInfo->getJobnum());
    ui->le_name->setText(m_operInfo->getName());
    ui->le_department->setText(m_operInfo->getDepartment());
    ui->le_job->setText(m_operInfo->getJob());
}

void TeacherMS::on_pb_modify_clicked()
{
    m_operData = Oper_Mdy;
    ui->le_name->setEnabled(true);
    ui->le_department->setEnabled(true);
    ui->le_job->setEnabled(true);
}

void TeacherMS::on_pb_delete_clicked()
{
    m_operData = Oper_Del;
    ui->le_name->setEnabled(true);
    ui->le_department->setEnabled(true);
    ui->le_job->setEnabled(true);
}

void TeacherMS::on_pb_add_clicked()
{
    m_operData = Oper_Add;
    ui->le_jobnum->setEnabled(true);
    ui->le_name->setEnabled(true);
    ui->le_department->setEnabled(true);
    ui->le_job->setEnabled(true);
}

void TeacherMS::on_pb_cancel_clicked()
{
    m_operData = Oper_None;
    ui->le_jobnum->setEnabled(true);
    ui->le_name->setEnabled(true);
    ui->le_department->setEnabled(true);
    ui->le_job->setEnabled(true);
}

void TeacherMS::on_pb_save_clicked()
{
    if(m_operData == Oper_Mdy)
    {
        m_operInfo->setJobnum(ui->le_jobnum->text());
        m_operInfo->setName(ui->le_name->text());
        m_operInfo->setDepartment(ui->le_department->text());
        m_operInfo->setJob(ui->le_job->text());
    }else if(m_operData == Oper_Del)
    {
        m_infoList.removeOne(*m_operInfo);
    }else if(m_operData == Oper_Add)
    {
        TeacherInfo info(ui->le_jobnum->text(), ui->le_name->text(),
                         ui->le_department->text(), ui->le_job->text());
        m_infoList.append(info);
    }
    on_pb_search_clicked();
    saveFileInfo();
}

bool operator==(const TeacherInfo &info1, const TeacherInfo &info2)
{
    return &info1.getJobnum() == &info2.getJobnum();
}
