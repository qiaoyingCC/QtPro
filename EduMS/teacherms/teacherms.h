#ifndef TEACHERMS_H
#define TEACHERMS_H

#include <QWidget>
#include <QMessageBox>

#include "teacherinfo.h"

namespace Ui {
class TeacherMS;
}

class TeacherMS : public QWidget
{
    Q_OBJECT

public:
    explicit TeacherMS(QWidget *parent = 0);
    ~TeacherMS();

    enum Search_Condition{
        Search_None = 0,
        Search_Jobnum,
        Search_Name,
        Search_Department,
        Search_Job
    };

    enum Oper_Data{
        Oper_None = 0,
        Oper_Mdy,
        Oper_Del,
        Oper_Add,
    };

    void readFileInfo();
    void updataTableInfos();
    void saveFileInfo();

    friend bool operator==(const TeacherInfo &info1, const TeacherInfo &info2);

private slots:
    void on_cb_condition_currentIndexChanged(int index);
    void on_pb_search_clicked();
    void on_tableWidget_clicked(const QModelIndex &index);
    void on_pb_modify_clicked();
    void on_pb_delete_clicked();
    void on_pb_add_clicked();
    void on_pb_cancel_clicked();
    void on_pb_save_clicked();

private:
    Ui::TeacherMS *ui;

    TeacherInfoList m_infoList;
    TeacherPtrInfoList m_infoPtrList;
    int m_SearchCond;
    int m_currentRow;
    TeacherInfo *m_operInfo;
    int m_operData;
};

#endif // TEACHERMS_H
