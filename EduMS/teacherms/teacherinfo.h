#ifndef TEACHERINFO_H
#define TEACHERINFO_H

#include <QString>

class TeacherInfo
{
public:
    TeacherInfo();
    TeacherInfo(const QString &jobnum, const QString &name,
                const QString &department, const QString &job);
    ~TeacherInfo();
    void display(void) const;
    void setJobnum(const QString &jobnum);
    const QString &getJobnum() const;
    void setName(const QString &name);
    const QString &getName() const;
    void setDepartment(const QString &department);
    const QString &getDepartment() const;
    void setJob(const QString &job);
    const QString &getJob() const;

private:
    QString m_jobnum;
    QString m_name;
    QString m_department;
    QString m_job;
};

typedef QList<TeacherInfo> TeacherInfoList;
typedef QList<TeacherInfo *> TeacherPtrInfoList;

#endif // TEACHERINFO_H
