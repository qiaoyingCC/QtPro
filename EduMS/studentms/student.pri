INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

FORMS += \
    $$PWD/studentms.ui

HEADERS += \
    $$PWD/studentinfo.h \
    $$PWD/studentms.h

SOURCES += \
    $$PWD/studentinfo.cpp \
    $$PWD/studentms.cpp