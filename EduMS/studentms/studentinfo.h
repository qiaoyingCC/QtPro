#ifndef _STUDENTINFO_H
#define _STUDENTINFO_H

#include <QString>
#include <QList>

using namespace std;

class StudentInfo
{
public:
    StudentInfo();
    StudentInfo(const QString &id, const QString &name,
    const QString &degree, const QString &major);
    void display(void) const;
    void setID(const QString &id);
    void setName(const QString &name);
    void setDegree(const QString &degree);
    void setMajor(const QString &major);
    const QString &getID() const;
    const QString &getName() const;
    const QString &getDegree() const;
    const QString &getMajor() const;

private:
    QString m_id;
    QString m_name;
    QString m_degree;
    QString m_major;
};

typedef QList<StudentInfo> StudentInfoList;
typedef QList<StudentInfo *> StudentPtrInfoList;

#endif // _STUDENTINFO_H

