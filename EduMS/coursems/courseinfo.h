#ifndef _COURSEINFO_H
#define _COURSEINFO_H

#include <QString>
#include <QList>

class CourseInfo
{
public:
    CourseInfo();
    CourseInfo(const QString &cournum, const QString &courname,
               const QString &genre, const QString &classhour);
    void display(void) const;
    void setCournum(const QString &cournum);
    void setCourname(const QString &courname);
    void setGenre(const QString &genre);
    void setClasshour(const QString &classhour);
    const QString &getCournum() const;
    const QString &getCourname() const;
    const QString &getGenre() const;
    const QString &getClassshour() const;
private:
    QString m_cournum;
    QString m_courname;
    QString m_genre;
    QString m_classhour;
};

typedef QList<CourseInfo> CourseInfoList;
typedef QList<CourseInfo *> CourseInfoPtrList;

#endif  //_COURSEINFO_H

