INCLUDEPATH += $$PWD
DEPENDPATH += $$PWD

FORMS += \
    $$PWD/coursems.ui

HEADERS += \
    $$PWD/courseinfo.h \
    $$PWD/coursems.h

SOURCES += \
    $$PWD/courseinfo.cpp \
    $$PWD/coursems.cpp