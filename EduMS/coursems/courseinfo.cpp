#include "courseinfo.h"

#include <QDebug>

CourseInfo::CourseInfo()
{
    m_cournum.clear();
    m_courname.clear();
    m_genre.clear();
    m_classhour.clear();
}

CourseInfo::CourseInfo(const QString &cournum, const QString &courname,
                       const QString &genre, const QString &classhour)
{
    m_cournum = cournum;
    m_courname = courname;
    m_genre = genre;
    m_classhour = classhour;
}

void CourseInfo::display(void) const
{
    qDebug() << "Cournum: " << m_cournum;
    qDebug() << "Courname: " << m_courname;
    qDebug() << "Genre: " << m_genre;
    qDebug() << "Classhour: " << m_classhour;
}

void CourseInfo::setCournum(const QString &cournum)
{
    m_cournum = cournum;
}

void CourseInfo::setCourname(const QString &courname)
{
    m_courname = courname;
}

void CourseInfo::setGenre(const QString &genre)
{
    m_genre = genre;
}

void CourseInfo::setClasshour(const QString &classhour)
{
    m_classhour = classhour;
}

const QString &CourseInfo::getCournum() const
{
    return m_cournum;
}

const QString &CourseInfo::getCourname() const
{
    return m_courname;
}

const QString &CourseInfo::getGenre() const
{
    return m_genre;
}

const QString &CourseInfo::getClassshour() const
{
    return m_classhour;
}
