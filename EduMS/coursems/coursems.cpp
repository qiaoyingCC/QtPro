#include "coursems.h"
#include "ui_coursems.h"

#include <QDebug>
#include <QFile>
#include <QTextStream>

CourseMs::CourseMs(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CourseMs)
{
    ui->setupUi(this);

    m_searchCond = Search_None;
    m_operData = Oper_None;

    readFileInfo();
    ui->le_condition->setEnabled(false);
}

CourseMs::~CourseMs()
{
    saveFileInfo();
    delete ui;
}

void CourseMs::readFileInfo()
{
    m_infoList.clear();
//    qDebug() << "readFileInfo()";
    QFile f("../CourseMs/data/course.txt");
    if(f.open(QIODevice::ReadOnly))
    {
        QTextStream t(&f);
        QString line;
        QStringList list;
        while(!t.atEnd())
        {
            line.clear();
            list.clear();
            line = t.readLine();
            list = line.split("\t\t");
            CourseInfo info(list.at(0), list.at(1), list.at(2), list.at(3));
            m_infoList.append(info);

        }
        f.close();
    }
}

void CourseMs::updataTableInfos(void)
{
    ui->tableWidget->clear();
    ui->tableWidget->setColumnCount(4);
    ui->tableWidget->setRowCount(m_infoPtrList.length());

    QStringList headers;
    headers << "课程号" << "名称" << "类别" << "课时";
    ui->tableWidget->setHorizontalHeaderLabels(headers);
    for(int i = 0; i < m_infoPtrList.count(); i++)
    {
        QTableWidgetItem *item = new QTableWidgetItem(m_infoPtrList.at(i)->getCournum());
        ui->tableWidget->setItem(i, 0, item);
        item = new QTableWidgetItem(m_infoPtrList.at(i)->getCourname());
        ui->tableWidget->setItem(i, 1, item);
        item = new QTableWidgetItem(m_infoPtrList.at(i)->getGenre());
        ui->tableWidget->setItem(i, 2, item);
        item = new QTableWidgetItem(m_infoPtrList.at(i)->getClassshour());
        ui->tableWidget->setItem(i, 3, item);
    }
}

void CourseMs::saveFileInfo()
{
    QFile f("../CourseMs/data/course.txt");
    if(f.open(QIODevice::WriteOnly | QIODevice::Truncate))
    {
        QTextStream t(&f);
        foreach(const CourseInfo &info, m_infoList)
        {
            t << info.getCournum() << "\t\t" << info.getCourname() << "\t\t"
                << info.getGenre() << "\t\t" << info.getClassshour() << "\n";
        }
        f.close();
    }
}

void CourseMs::on_cb_condition_currentIndexChanged(int index)
{
    m_searchCond = index;
    if(m_searchCond == 0)
    {
        ui->le_condition->setEnabled(false);
    }else
    {
        ui->le_condition->setEnabled(true);
    }
}

void CourseMs::on_pb_search_clicked()
{
    m_infoPtrList.clear();
    if(m_searchCond == Search_Cournum)
    {
        for(int i = 0; i < m_infoList.length(); i++)
        {
            if(ui->le_condition->text() == m_infoList.at(i).getCournum())
            {
                m_infoPtrList.append(&(m_infoList[i]));
            }
        }
    }else if(m_searchCond == Search_Courname)
    {
        for(int i = 0; i < m_infoList.length(); i++)
        {
            if(ui->le_condition->text() == m_infoList.at(i).getCourname())
            {
                m_infoPtrList.append(&(m_infoList[i]));
            }
        }
    }else if(m_searchCond == Search_Genre)
    {
        for(int i = 0; i < m_infoList.length(); i++)
        {
            if(ui->le_condition->text() == m_infoList.at(i).getGenre())
            {
                m_infoPtrList.append(&(m_infoList[i]));
            }
        }
    }else if(m_searchCond == Search_Classhour)
    {
        for(int i = 0; i < m_infoList.length(); i++)
        {
            if(ui->le_condition->text() == m_infoList.at(i).getClassshour())
            {
                m_infoPtrList.append(&(m_infoList[i]));
            }
        }
    }else
    {
        for(int i = 0; i < m_infoList.length(); i++)
        {
            m_infoPtrList.append(&(m_infoList[i]));
        }
    }
    updataTableInfos();
}

void CourseMs::on_tableWidget_clicked(const QModelIndex &index)
{
    m_currentRow = index.row();
    m_operInfo = m_infoPtrList.at(m_currentRow);
    ui->le_cournum->setText(m_operInfo->getCournum());
    ui->le_courname->setText(m_operInfo->getCourname());
    ui->le_genre->setText(m_operInfo->getGenre());
    ui->le_classhour->setText(m_operInfo->getClassshour());
}

void CourseMs::on_pb_modify_clicked()
{
    m_operData = Oper_Mdy;
    ui->le_cournum->setEnabled(true);
    ui->le_courname->setEnabled(true);
    ui->le_genre->setEnabled(true);
    ui->le_classhour->setEnabled(true);
}

void CourseMs::on_pb_delete_clicked()
{
    m_operData = Oper_Del;
    ui->le_cournum->setEnabled(true);
    ui->le_courname->setEnabled(true);
    ui->le_genre->setEnabled(true);
    ui->le_classhour->setEnabled(true);
}

void CourseMs::on_pb_add_clicked()
{
    m_operData = Oper_Add;
    ui->le_cournum->setEnabled(true);
    ui->le_courname->setEnabled(true);
    ui->le_genre->setEnabled(true);
    ui->le_classhour->setEnabled(true);
}

void CourseMs::on_pb_cancel_clicked()
{
    m_operData = Oper_None;
    ui->le_cournum->setEnabled(false);
    ui->le_cournum->setEnabled(false);
    ui->le_genre->setEnabled(false);
    ui->le_classhour->setEnabled(false);
}

void CourseMs::on_pb_save_clicked()
{
    if(m_operData == Oper_Mdy)
    {
        m_operInfo->setCournum(ui->le_cournum->text());
        m_operInfo->setCourname(ui->le_courname->text());
        m_operInfo->setGenre(ui->le_genre->text());
        m_operInfo->setClasshour(ui->le_classhour->text());
    }else if(m_operData == Oper_Del)
    {
        m_infoList.removeOne(*m_operInfo);
    }else if(m_operData == Oper_Add)
    {
        CourseInfo info(ui->le_cournum->text(), ui->le_courname->text(),
                        ui->le_genre->text(), ui->le_classhour->text());
        m_infoList.append(info);
    }
    on_pb_search_clicked();
    saveFileInfo();
}

bool operator ==(const CourseInfo &info1, const CourseInfo &info2)
{
    return info1.getCournum() == info2.getCournum();
}
