#include "calculator.h"
#include "ui_calculator.h"
#include <QDebug>

Calculator::Calculator(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Calculator)
{
    ui->setupUi(this);

    m_exp.resize(100);
    m_exp.clear();
}

Calculator::~Calculator()
{
    delete ui;
}

void Calculator::on_pb_num0_clicked()
{
    m_exp += "0";
    ui->lb_expression->setText(m_exp);
}

void Calculator::on_pb_num1_clicked()
{
    m_exp += "1";
    ui->lb_expression->setText(m_exp);
}

void Calculator::on_pb_num2_clicked()
{
    m_exp += "2";
    ui->lb_expression->setText(m_exp);
}

void Calculator::on_pb_num3_clicked()
{
    m_exp += "3";
    ui->lb_expression->setText(m_exp);
}

void Calculator::on_pb_num4_clicked()
{
    m_exp += "4";
    ui->lb_expression->setText(m_exp);
}

void Calculator::on_pb_num5_clicked()
{
    m_exp += "5";
    ui->lb_expression->setText(m_exp);
}

void Calculator::on_pb_num6_clicked()
{
    m_exp += "6";
    ui->lb_expression->setText(m_exp);
}

void Calculator::on_pb_num7_clicked()
{
    m_exp += "7";
    ui->lb_expression->setText(m_exp);
}

void Calculator::on_pb_num8_clicked()
{
    m_exp += "8";
    ui->lb_expression->setText(m_exp);
}

void Calculator::on_pb_num9_clicked()
{
    m_exp += "9";
    ui->lb_expression->setText(m_exp);
}

void Calculator::on_pb_add_clicked()
{
    m_exp += " + ";
    ui->lb_expression->setText(m_exp);
}

void Calculator::on_pb_sub_clicked()
{
    m_exp += " - ";
    ui->lb_expression->setText(m_exp);
}

void Calculator::on_pb_mul_clicked()
{
    m_exp += " * ";
    ui->lb_expression->setText(m_exp);
}

void Calculator::on_pb_div_clicked()
{
    m_exp += " / ";
    ui->lb_expression->setText(m_exp);
}

void Calculator::on_pb_equ_2_clicked()
{
    int i = 1;
    QStringList list = m_exp.split(" ");
    qDebug() << list;
    QStringList list1;
    double value = list.at(0).toDouble();
    for(i = 1; i < list.length(); i = i + 2)
    {
          if(list.at(i) == "+")
          {
              list1.append(QString::number(value));
              list1.append("+");
              value = list.at(i + 1).toDouble();
          }else if(list.at(i) == "-")
          {
              list1.append(QString::number(value));
              list1.append("-");
              value = list.at(i + 1).toDouble();
          }else if(list.at(i) == "*")
          {
              value *= list.at(i + 1).toDouble();
          }else if(list.at(i) == "/")
          {
              value /= list.at(i + 1).toDouble();
          }
    }
    list1.append(QString::number(value));
    double result = list1.at(0).toDouble();
    qDebug() << result;
    qDebug() << list1;
    for(i = 1; i < list1.length(); i = i + 2)
    {
          if(list1.at(i) == "+")
          {
              result += list1.at(i + 1).toDouble();
          }else if(list1.at(i) == "-")
          {
              result -= list1.at(i + 1).toDouble();
          }
    }
    qDebug() << list1;
    ui->lb_result->setText(QString::number(result));
    //ui->lb_expression->setText(m_exp);
//    m_exp.clear();
//    result.clear();
}

//void Calculator::paintEvent(QPaintEvent *)
//{
//    qDebug() << "paintEvent" << this->rect();
//    QPainter painter(this);
//    painter.setPen(Qt::blue);
//    painter.setFont(QFont("Arial", 30));
//    painter.drawText(rect(), Qt::AlignCenter, "Qt");
//}

void Calculator::keyPressEvent(QKeyEvent *ev)
{
    qDebug() << "keyPressEvent: " << ev->key();
    switch(ev->key())
    {
    case Qt::Key_0: on_pb_num0_clicked(); break;
    case Qt::Key_1: on_pb_num1_clicked(); break;
    case Qt::Key_2: on_pb_num2_clicked(); break;
    case Qt::Key_3: on_pb_num3_clicked(); break;
    case Qt::Key_4: on_pb_num4_clicked(); break;
    case Qt::Key_5: on_pb_num5_clicked(); break;
    case Qt::Key_6: on_pb_num6_clicked(); break;
    case Qt::Key_7: on_pb_num7_clicked(); break;
    case Qt::Key_8: on_pb_num8_clicked(); break;
    case Qt::Key_9: on_pb_num9_clicked(); break;
    case Qt::Key_Plus: on_pb_add_clicked(); break;
    case Qt::Key_Minus: on_pb_sub_clicked(); break;
    case Qt::Key_Asterisk: on_pb_mul_clicked(); break;
    case Qt::Key_Slash: on_pb_add_clicked(); break;
    case Qt::Key_Equal: on_pb_equ_2_clicked(); break;
    case Qt::Key_Enter: on_pb_equ_2_clicked(); break;
    case Qt::Key_Return: on_pb_equ_2_clicked(); break;
    default:
    QWidget::keyPressEvent(ev);
    }
}

