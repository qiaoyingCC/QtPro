#ifndef CALCULATOR_H
#define CALCULATOR_H

#include <QWidget>
#include <QPainter>
#include <QKeyEvent>

namespace Ui {
class Calculator;
}

class Calculator : public QWidget
{
    Q_OBJECT

public:
    explicit Calculator(QWidget *parent = 0);
    ~Calculator();

private slots:
    void on_pb_num0_clicked();
    void on_pb_num1_clicked();
    void on_pb_num2_clicked();
    void on_pb_num3_clicked();
    void on_pb_num4_clicked();
    void on_pb_num5_clicked();
    void on_pb_num6_clicked();
    void on_pb_num7_clicked();
    void on_pb_num8_clicked();
    void on_pb_num9_clicked();

    void on_pb_add_clicked();
    void on_pb_sub_clicked();
    void on_pb_mul_clicked();
    void on_pb_div_clicked();

    void on_pb_equ_2_clicked();

protected:
//    void paintEvent(QPaintEvent *);
    void keyPressEvent(QKeyEvent *ev);

private:
    Ui::Calculator *ui;

    QString m_exp;
};

#endif // CALCULATOR_H
