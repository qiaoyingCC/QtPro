#-------------------------------------------------
#
# Project created by QtCreator 2019-05-03T14:21:39
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Calculator
TEMPLATE = app


SOURCES += main.cpp\
        calculator.cpp

HEADERS  += calculator.h

FORMS    += calculator.ui
