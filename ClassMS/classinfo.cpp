#include "classinfo.h"

#include <QDebug>

ClassInfo::ClassInfo()
{
    m_classid.clear();
    m_definition.clear();
    m_classroom.clear();
    m_number.clear();
}

ClassInfo::ClassInfo(const QString &classid, const QString &definition,
                     const QString &classroom, const QString &number)
{
    m_classid = classid;
    m_definition = definition;
    m_classroom = classroom;
    m_number = number;
}

ClassInfo::~ClassInfo()
{

}

void ClassInfo::display() const
{
    qDebug() << "Classid: " << m_classid;
    qDebug() << "Definition: " << m_definition;
    qDebug() << "Classroom: " << m_classroom;
    qDebug() << "Number: " << m_number;
}

void ClassInfo::setClassid(const QString &classid)
{
    m_classid = classid;
}

const QString &ClassInfo::getClassid() const
{
    return m_classid;
}

void ClassInfo::setDefinition(const QString &definition)
{
    m_definition = definition;
}

const QString &ClassInfo::getDefinition() const
{
    return m_definition;
}

void ClassInfo::setClassroom(const QString &classroom)
{
    m_classroom = classroom;
}

const QString &ClassInfo::getClassroom() const
{
    return m_classroom;
}

void ClassInfo::setNumber(const QString &number)
{
    m_number = number;
}

const QString &ClassInfo::getNumber() const
{
    return m_number;
}
