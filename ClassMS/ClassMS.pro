#-------------------------------------------------
#
# Project created by QtCreator 2019-05-16T20:37:22
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ClassMS
TEMPLATE = app


SOURCES += main.cpp\
        classms.cpp \
    classinfo.cpp

HEADERS  += classms.h \
    classinfo.h

FORMS    += classms.ui
