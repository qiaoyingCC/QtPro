#ifndef CLASSINFO_H
#define CLASSINFO_H

#include <QString>

class ClassInfo
{
public:
    ClassInfo();
    ClassInfo(const QString &classid, const QString &definition,
              const QString &classroom, const QString &number);
    ~ClassInfo();
    void display() const;
    void setClassid(const QString &classid);
    const QString &getClassid() const;
    void setDefinition(const QString &definition);
    const QString &getDefinition() const;
    void setClassroom(const QString &classroom);
    const QString &getClassroom() const;
    void setNumber(const QString &number);
    const QString &getNumber() const;

private:
    QString m_classid;
    QString m_definition;
    QString m_classroom;
    QString m_number;
};

typedef QList<ClassInfo> ClassInfoList;
typedef QList<ClassInfo *> ClassInfoPtrList;

#endif // CLASSINFO_H
