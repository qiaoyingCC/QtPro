#ifndef STUDENTMS_H
#define STUDENTMS_H

#include <QWidget>
#include "studentinfo.h"


namespace Ui {
class StudentMS;
}

class StudentMS : public QWidget
{
    Q_OBJECT


public:
    explicit StudentMS(QWidget *parent = 0);
    ~StudentMS();

    friend bool operator==(const StudentInfo &info1, const StudentInfo &info2);

    enum Search_Condition{
        Search_None = 0,
        Search_ID,
        Search_Name,
        Seatch_Degree,
        Search_Major
    };

    enum Oper_Data{
        Oper_None,
        Oper_Add,
        Oper_Del,
        Oper_Mdy
    };

    void readFileInfo(void);
    void updateTableInfos(void);
    void saveFileInfo(void);

private slots:
    void on_cb_condition_currentIndexChanged(int index);

    void on_pb_search_clicked();
    void on_tableWidget_clicked(const QModelIndex &index);
    void on_pb_modify_clicked();
    void on_pb_delete_clicked();
    void on_pb_add_clicked();
    void on_pb_cancel_clicked();
    void on_pb_save_clicked();


private:
    Ui::StudentMS *ui;

    StudentInfoList m_infoList;

    int m_searchCond;
    int m_currentRow;
    int m_operData;
    StudentInfo * m_operInfo;
    StudentPtrInfoList m_infoPtrList;

};

#endif // STUDENTMS_H
