#-------------------------------------------------
#
# Project created by QtCreator 2019-05-12T09:40:44
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = StudentMS
TEMPLATE = app


SOURCES += main.cpp\
        studentms.cpp \
    studentinfo.cpp

HEADERS  += studentms.h \
    studentinfo.h

FORMS    += studentms.ui
