#include "studentms.h"
#include "ui_studentms.h"

#include <QDebug>
#include <QFile>
#include <QTextStream>
#include <QMessageBox>


StudentMS::StudentMS(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::StudentMS)
{
    ui->setupUi(this);

    m_searchCond = Search_None;
    ui->le_condition->setEnabled(false);
    m_operData = Oper_None;

    readFileInfo();
}

StudentMS::~StudentMS()
{
    saveFileInfo();
    delete ui;
}

void StudentMS::readFileInfo(void)
{
    m_infoList.clear();
    QFile f("../StudentMS/data/student.txt");
    if(f.open(QIODevice::ReadOnly))
    {
        QTextStream t(&f);
        QString line;
        QStringList list;
        while(!t.atEnd())
        {
            line.clear();
            list.clear();
            line = t.readLine();
            list = line.split("\t\t");
            StudentInfo info(list.at(0), list.at(1), list.at(2), list.at(3));
            m_infoList.append(info);
        }
        f.close();
    }
//    for(int i = 0; i < m_infoList.length(); i++)
//    {
//        m_infoList.at(i).display();
//    }
}

void StudentMS::updateTableInfos(void)
{
    ui->tableWidget->clear();  //清空tableWidget
    ui->tableWidget->setColumnCount(4);  //设置列数
    ui->tableWidget->setRowCount(m_infoPtrList.length());  //设置行数

    // set header lables(设置第一列的头名称)
    QStringList headers;
    headers << "学号" << "姓名" << "年级" << "专业";
    ui->tableWidget->setHorizontalHeaderLabels(headers);
    for(int i=0; i < m_infoPtrList.count(); i++)
    {
        QTableWidgetItem *item = new QTableWidgetItem(m_infoPtrList.at(i)->getID());
        ui->tableWidget->setItem(i, 0, item);
        item = new QTableWidgetItem(m_infoPtrList.at(i)->getName());
        ui->tableWidget->setItem(i, 1, item);
        item = new QTableWidgetItem(m_infoPtrList.at(i)->getDegree());
        ui->tableWidget->setItem(i, 2, item);
        item = new QTableWidgetItem(m_infoPtrList.at(i)->getMajor());
        ui->tableWidget->setItem(i, 3, item);
    }
}

void StudentMS::saveFileInfo(void)
{
    QFile f("../StudentMS/data/student.txt");
    if(f.open(QIODevice::WriteOnly | QIODevice::Truncate))
    {
        QTextStream t(&f);
        foreach(const StudentInfo &info, m_infoList)
        {
            t << info.getID() << "\t\t" << info.getName() << "\t\t"
              << info.getDegree() << "\t\t" << info.getMajor() << "\n";
        }
        f.close();
    }
}

void StudentMS::on_cb_condition_currentIndexChanged(int index)
{
    m_searchCond = index;
    if(m_searchCond == 0)
    {
        ui->le_condition->setEnabled(false);
    }else
    {
        ui->le_condition->setEnabled(true);
    }
}

void StudentMS::on_pb_search_clicked()
{
    if(ui->le_condition->text().isEmpty() && ui->le_condition->isEnabled())
    {
        QMessageBox messagebox;
        messagebox.setWindowTitle("提示！");
        messagebox.setText("条件不能为空！");
        messagebox.exec();
    }else
    {
        m_infoPtrList.clear();
        if(m_searchCond == Search_ID)
        {
            for(int i = 0; i < m_infoList.length(); i++)
            {
                if(ui->le_condition->text() == m_infoList.at(i).getID())
                {
                    m_infoPtrList.append(&(m_infoList[i]));
                }
            }
        }else if(m_searchCond == Search_Name)
        {
            for(int i = 0; i < m_infoList.length(); i++)
            {
                if(ui->le_condition->text() == m_infoList.at(i).getName())
                {
                    m_infoPtrList.append(&(m_infoList[i]));
                }
            }
        }else if(m_searchCond == Seatch_Degree)
        {
            for(int i = 0; i < m_infoList.length(); i++)
            {
                if(ui->le_condition->text() == m_infoList.at(i).getDegree())
                {
                    m_infoPtrList.append(&(m_infoList[i]));
                }
            }
        }else if(m_searchCond == Search_Major)
        {
            for(int i = 0; i < m_infoList.length(); i++)
            {
                if(ui->le_condition->text() == m_infoList.at(i).getMajor())
                {
                    m_infoPtrList.append(&(m_infoList[i]));
                }
            }
        }else
        {
            for(int i = 0; i < m_infoList.length(); i++)
            {
                m_infoPtrList.append(&(m_infoList[i]));
            }
        }
        updateTableInfos(); //注意函数中m_infoList换成m_infoPtrList
    }
}

void StudentMS::on_tableWidget_clicked(const QModelIndex &index)
{
    m_currentRow = index.row();
    m_operInfo = m_infoPtrList.at(m_currentRow);
    ui->le_id->setText(m_operInfo->getID());
    ui->le_name->setText(m_operInfo->getName());
    ui->le_degree->setText(m_operInfo->getDegree());
    ui->le_major->setText(m_operInfo->getMajor());
}


void StudentMS::on_pb_modify_clicked()
{
    m_operData = Oper_Mdy;
    ui->le_name->setEnabled(true);
    ui->le_degree->setEnabled(true);
    ui->le_major->setEnabled(true);
    ui->pb_save->setEnabled(true);
}

void StudentMS::on_pb_delete_clicked()
{
    m_operData = Oper_Del;
    ui->le_name->setEnabled(true);
    ui->le_degree->setEnabled(true);
    ui->le_major->setEnabled(true);
}

void StudentMS::on_pb_add_clicked()
{
    m_operData = Oper_Add;
    ui->le_id->setEnabled(true);
    ui->le_name->setEnabled(true);
    ui->le_degree->setEnabled(true);
    ui->le_major->setEnabled(true);
}

void StudentMS::on_pb_cancel_clicked()
{
    m_operData = Oper_None;
    ui->le_name->setEnabled(false);
    ui->le_degree->setEnabled(false);
    ui->le_major->setEnabled(false);
}

void StudentMS::on_pb_save_clicked()
{
    if(m_operData == Oper_Mdy)
    {
        m_operInfo->setID(ui->le_id->text());
        m_operInfo->setName(ui->le_name->text());
        m_operInfo->setDegree(ui->le_degree->text());
        m_operInfo->setMajor(ui->le_major->text());
    }else if(m_operData == Oper_Del)
    {
        m_infoList.removeOne(*m_operInfo);
    }else if(m_operData == Oper_Add)
    {
        StudentInfo info(ui->le_id->text(),
        ui->le_name->text(),
        ui->le_degree->text(),
        ui->le_major->text());
        m_infoList.append(info);
    }
    on_pb_search_clicked();
    saveFileInfo();
}

bool operator==(const StudentInfo &info1, const StudentInfo &info2)
{
    return info1.getID() == info2.getID();
}
