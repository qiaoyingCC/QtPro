#ifndef TCPCLIENT_H
#define TCPCLIENT_H

#include <QWidget>
#include <QTcpSocket>

namespace Ui {
class TcpClient;
}

class TcpClient : public QWidget
{
    Q_OBJECT


public slots:
    void readyToRead();

public:
    explicit TcpClient(QWidget *parent = 0);
    ~TcpClient();

private:
    Ui::TcpClient *ui;

    quint16 m_tcpBlockSize;
    QTcpSocket *m_socket;
};

#endif // TCPCLIENT_H
