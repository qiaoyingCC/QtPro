#include "tcpclient.h"
#include "ui_tcpclient.h"

#include <QTime>
#include <QDebug>

TcpClient::TcpClient(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TcpClient)
{
    ui->setupUi(this);

    m_tcpBlockSize = 0;
    m_socket = new QTcpSocket(this);
    connect(m_socket, SIGNAL(readyRead()),
            this, SLOT(readyToRead()));
    m_socket->connectToHost("localhost", 55555);
}

void TcpClient::readyToRead()
{
//    qDebug() << "readyToRead(): " << QTime::currentTime();
    QDataStream in(m_socket);
    in.setVersion(QDataStream::Qt_4_6);
    if(m_tcpBlockSize == 0)
    {
    if(m_socket->bytesAvailable()<sizeof(quint16))
    return;
    in >> m_tcpBlockSize;
    }
    if(m_socket->bytesAvailable() < m_tcpBlockSize)
    return;
    QString greeting;
    in >> greeting;
    ui->label->setText(greeting);
    m_tcpBlockSize = 0;
}

TcpClient::~TcpClient()
{
    delete ui;
}
