#ifndef TEXTEDITOR_H
#define TEXTEDITOR_H

#include <QMainWindow>
#include <QCloseEvent>

namespace Ui {
class TextEditor;
}

class TextEditor : public QMainWindow
{
    Q_OBJECT

protected:
    void closeEvent(QCloseEvent *ev);

public:
    explicit TextEditor(QWidget *parent = 0);
    ~TextEditor();

private slots:
    void on_actionOpen_triggered();
    void on_actionSave_triggered();
    void on_actionSave_as_triggered();

    void on_textEdit_textChanged();


    void on_actionHelp_triggered();

    void on_actionFont_triggered();

    void on_actionSize_triggered();

private:
    Ui::TextEditor *ui;

    QString m_fileName;
    bool m_isChanged;
};

#endif // TEXTEDITOR_H
