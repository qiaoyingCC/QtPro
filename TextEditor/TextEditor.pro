#-------------------------------------------------
#
# Project created by QtCreator 2019-05-11T15:52:51
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TextEditor
TEMPLATE = app


SOURCES += main.cpp\
        texteditor.cpp

HEADERS  += texteditor.h

FORMS    += texteditor.ui

RESOURCES += \
    icons.qrc \
    languages.qrc

TRANSLATIONS += en2ch.ts

DISTFILES += \
    en2ch.ts
