#include "texteditor.h"
#include "ui_texteditor.h"

#include <QDebug>
#include <QFileDialog>
#include <QFile>
#include <QTextStream>
#include <QMessageBox>

#include <QFontDialog>
#include <QColorDialog>

TextEditor::TextEditor(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::TextEditor)
{
    ui->setupUi(this);

    this->setCentralWidget(ui->textEdit);

    m_isChanged = false;
    m_fileName.clear();
}

TextEditor::~TextEditor()
{
    delete ui;
}

void TextEditor::on_actionOpen_triggered()
{
//    qDebug() << "TextEditor::on_actionOpen_triggered()";
    m_fileName = QFileDialog::getOpenFileName(this,
        tr("Open Code"), "./", tr("Code Files (*.*)"));
    QFile f(m_fileName);
    if(f.open(QIODevice::ReadOnly))
    {
        QTextStream t(&f);
        ui->textEdit->setText(t.readAll());
    }
}

void TextEditor::on_actionSave_triggered()
{
    if(m_fileName.isEmpty())
    {
        on_actionSave_as_triggered();
    }else
    {
        QFile f(m_fileName);
        if(f.open(QIODevice::WriteOnly | QIODevice::Truncate))
        {
            QTextStream t(&f);
            t << ui->textEdit->toPlainText();
            f.close();
        }
    }
}

void TextEditor::on_actionSave_as_triggered()
{
    qDebug() << "TextEditor::on_actionSave_as_triggered()";
    m_fileName = QFileDialog::getSaveFileName(this,
        tr("Save Code"), "./", tr("Code Files (*.*)"));
    qDebug() << m_fileName;
    QFile f(m_fileName);
    if(f.open(QIODevice::WriteOnly | QIODevice::Truncate))
    {
        QTextStream t(&f);
        t << ui->textEdit->toPlainText();
        f.close();
    }
}

void TextEditor::closeEvent(QCloseEvent *ev)
{
    qDebug() << "closeEvent";
    if(m_isChanged)
    {
//        QMessageBox msgBox;
//        msgBox.setText("The document has been modified.");
//        msgBox.setInformativeText("Do you want to save your changes?");
//        msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
//        msgBox.setDefaultButton(QMessageBox::Save);
        int ret = QMessageBox::question(this, "小问题", "The document has been modified. \n Do you want to save your changes?",
                                        QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel,
                                        QMessageBox::Save);
        switch (ret) {
          case QMessageBox::Save:
              on_actionSave_triggered();
              break;
          case QMessageBox::Discard:
              ev->accept();
              break;
          case QMessageBox::Cancel:
              ev->ignore();
              break;
          default:
              // should never be reached
              break;
        }
    }else
    {
        ev->accept();
    }
}

void TextEditor::on_textEdit_textChanged()
{
    qDebug() << "on_textEdit_textChanged";
    m_isChanged = true;
}

void TextEditor::on_actionHelp_triggered()
{
    QMessageBox::aboutQt(this);
}

void TextEditor::on_actionFont_triggered()
{
    bool ok;
    QFont font = QFontDialog::getFont(
                    &ok, QFont("Times", 10), this);
    if (ok) {
        qDebug() << font;
    } else {
        // the user canceled the dialog; font is set to the initial
        // value, in this case Helvetica [Cronyx], 10
    }
}

void TextEditor::on_actionSize_triggered()
{
    qDebug() << QColorDialog::getColor(Qt::red, this);
}
