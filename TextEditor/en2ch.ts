<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>TextEditor</name>
    <message>
        <location filename="texteditor.ui" line="14"/>
        <source>TextEditor</source>
        <translation type="unfinished">文件编译器</translation>
    </message>
    <message>
        <location filename="texteditor.ui" line="39"/>
        <source>File</source>
        <translation type="unfinished">文件(F)</translation>
    </message>
    <message>
        <location filename="texteditor.ui" line="51"/>
        <source>Edit</source>
        <translation type="unfinished">编辑(E)</translation>
    </message>
    <message>
        <location filename="texteditor.ui" line="63"/>
        <source>About</source>
        <translation type="unfinished">关于(A)</translation>
    </message>
    <message>
        <location filename="texteditor.ui" line="69"/>
        <source>View</source>
        <translation type="unfinished">视图(V)</translation>
    </message>
    <message>
        <location filename="texteditor.ui" line="101"/>
        <source>New</source>
        <translation type="unfinished">新建</translation>
    </message>
    <message>
        <location filename="texteditor.ui" line="104"/>
        <source>Ctrl+N</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="texteditor.ui" line="113"/>
        <source>Open</source>
        <translation type="unfinished">打开</translation>
    </message>
    <message>
        <location filename="texteditor.ui" line="116"/>
        <source>Ctrl+O</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="texteditor.ui" line="125"/>
        <source>Save</source>
        <translation type="unfinished">保存</translation>
    </message>
    <message>
        <location filename="texteditor.ui" line="128"/>
        <source>Ctrl+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="texteditor.ui" line="137"/>
        <source>Save as...</source>
        <translation type="unfinished">另存为</translation>
    </message>
    <message>
        <location filename="texteditor.ui" line="140"/>
        <source>Ctrl+Shift+S</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="texteditor.ui" line="149"/>
        <source>Close</source>
        <translation type="unfinished">关闭</translation>
    </message>
    <message>
        <location filename="texteditor.ui" line="152"/>
        <source>Ctrl+Q</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="texteditor.ui" line="161"/>
        <source>Undo</source>
        <translation type="unfinished">撤销</translation>
    </message>
    <message>
        <location filename="texteditor.ui" line="164"/>
        <source>Ctrl+Z</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="texteditor.ui" line="173"/>
        <source>Redo</source>
        <translation type="unfinished">返回</translation>
    </message>
    <message>
        <location filename="texteditor.ui" line="176"/>
        <source>Ctrl+Y</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="texteditor.ui" line="185"/>
        <source>Copy</source>
        <translation type="unfinished">复制</translation>
    </message>
    <message>
        <location filename="texteditor.ui" line="188"/>
        <source>Ctrl+C</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="texteditor.ui" line="197"/>
        <source>Cut</source>
        <translation type="unfinished">剪切</translation>
    </message>
    <message>
        <location filename="texteditor.ui" line="200"/>
        <source>Ctrl+X</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="texteditor.ui" line="209"/>
        <source>Paste</source>
        <translation type="unfinished">粘贴</translation>
    </message>
    <message>
        <location filename="texteditor.ui" line="212"/>
        <source>Ctrl+V</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="texteditor.ui" line="217"/>
        <source>Help</source>
        <translation type="unfinished">帮助</translation>
    </message>
    <message>
        <location filename="texteditor.ui" line="222"/>
        <source>Font</source>
        <translation type="unfinished">字体</translation>
    </message>
    <message>
        <location filename="texteditor.ui" line="227"/>
        <source>Size</source>
        <translation type="unfinished">大小</translation>
    </message>
    <message>
        <location filename="texteditor.cpp" line="34"/>
        <source>Open Code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="texteditor.cpp" line="34"/>
        <location filename="texteditor.cpp" line="64"/>
        <source>Code Files (*.*)</source>
        <translation type="unfinished">源文件</translation>
    </message>
    <message>
        <location filename="texteditor.cpp" line="64"/>
        <source>Save Code</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
