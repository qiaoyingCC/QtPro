#-------------------------------------------------
#
# Project created by QtCreator 2019-05-15T08:29:09
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CourseMS
TEMPLATE = app


SOURCES += main.cpp\
        coursems.cpp \
    courseinfo.cpp \

HEADERS  += coursems.h \
    courseinfo.h \

FORMS    += coursems.ui
