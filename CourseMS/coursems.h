#ifndef COURSEMS_H
#define COURSEMS_H

#include <QWidget>
#include "courseinfo.h"

namespace Ui {
class CourseMs;
}

class CourseMs : public QWidget
{
    Q_OBJECT

public:
    explicit CourseMs(QWidget *parent = 0);
    ~CourseMs();

    enum Search_Condition{
        Search_None = 0,
        Search_Cournum,
        Search_Courname,
        Search_Genre,
        Search_Classhour
    };

    enum Oper_Data{
        Oper_None,
        Oper_Add,
        Oper_Del,
        Oper_Mdy,
    };

    void readFileInfo();
    void updataTableInfos(void);
    void saveFileInfo();

    friend bool operator ==(const CourseInfo &info1, const CourseInfo &info2);

private slots:
    void on_cb_condition_currentIndexChanged(int index);
    void on_pb_search_clicked();
    void on_tableWidget_clicked(const QModelIndex &index);
    void on_pb_modify_clicked();
    void on_pb_delete_clicked();
    void on_pb_add_clicked();
    void on_pb_cancel_clicked();
    void on_pb_save_clicked();

private:
    Ui::CourseMs *ui;

    CourseInfoList m_infoList;
    CourseInfoPtrList m_infoPtrList;
    int m_searchCond;
    int m_currentRow;
    CourseInfo * m_operInfo;
    int m_operData;
};

#endif // COURSEMS_H
