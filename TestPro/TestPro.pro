QT += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QT += testlib

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

SOURCES +=  tst_testpro.cpp
