#include "widget.h"
#include "mainwindow.h"
#include <QApplication>
#include <QDebug>
//#include <QStringBuilder>
//#include <iostream>

//using namespace std;
int main(int argc, char *argv[])
//int main()
{
//    qint32 a = 100;
//    QString str1 = "i love";
//    QString str2 = "banana";
//    QString str3 = "and";
//    QString str4 = "apple";
//    qDebug() << a << ", " << str1;
//    qDebug() << a << ", " << str2;

//    QString str = str1 + " " + str2 + " " + str3 + " " + str4;
//    qDebug() << str;
//    QString st = str1 % " " % str2 % " " % str3 % " " % str4;
//    qDebug() << st;
//    QString qstr = QString("%1 + %2 + %3 + %4").arg(str1).arg(str2).arg(str3).arg(str4);
//    qDebug() << qstr;

//    QString s1 = QString::number(12); //数字12转为字符串"12"
//    qDebug() << s1;
//    QString s2 = QString::number(12,2); //将数字12转为二进制数字，以字符串形式输出
//    qDebug() << s2;

//    QString s2 = "123";
//    qDebug() << s2 << ", " << s2.toInt(); //字符串转为数字

//    QString s3 = "";
//    qDebug() << s3.isEmpty();
//    qDebug() << s3.isNull();

//    std::string s = "Hello world";
//    QString qs = QString::fromStdString(s); //STL转QString
//    qDebug() << qs;
//    std::string s1 = qs.toStdString(); //QString转STL
//    cout << s1 << endl;

//    QString whole = "Stockholm - Copenhagen - Oslo - Helsinki";
//    qDebug() << whole;
//    QStringList parts = whole.split(" - ");
//    QStringList parts1 = whole.split("-");
//    qDebug() << parts;
//    qDebug() << parts1;
//    QString wholeAgain = parts.join("**");
//    qDebug() << wholeAgain;

//    QStringList verbs;
//    verbs << "running" << "walking" << "compiling" << "linking";
//    qDebug() << verbs;
//    verbs.replaceInStrings("ing", "er");
//    qDebug() << verbs;

//    QStringList verbs;
//    verbs << "running" << "walking" << "compiling" << "linking";
//    for(int i=0; i<verbs.length(); ++i)
//    qDebug() << verbs[i];
//    qDebug() << "=================";

//    for(int i=0; i<verbs.length(); ++i)
//    qDebug() << verbs.at(i);
//    qDebug() << "=================";

//    foreach(const QString &str, verbs)
//    qDebug() << str;

//    QList<QString> verbs;
//    verbs << "running" << "walking" << "compiling" << "linking";
//    for(QList<QString>::iterator it = verbs.begin(); it != verbs.end(); it++)
//    {
//        qDebug() << *it;
//    }
//    qDebug() << "=================";
//    foreach(const QString &str, verbs)
//    qDebug() << str;


    QApplication a(argc, argv);
    Widget w;
    MainWindow m;


    QObject::connect(&w, SIGNAL(SignalsUserLogin(QString)),
                     &m, SLOT(SlotsUserLogin(QString)));
    w.show();

    return a.exec();
//    return 0;
}

