#include "widget.h"
#include "ui_widget.h"
#include <QDebug>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);

    connect(ui->lineEdit, SIGNAL(textChanged(QString)),
            ui->label, SLOT(setText(QString)));
    connect(ui->lineEdit_2, SIGNAL(textChanged(QString)),
            ui->label, SLOT(setText(QString)));
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_pushButton_2_clicked()
{
    qDebug() << "Widget::on_pushButton_2_clicked";
    emit SignalsUserLogin(ui->lineEdit->text());
    this->hide();
}
