#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::SlotsUserLogin(QString name)
{
    qDebug() << "MainWindow::SlotsUserLogin()";
    this->setWindowTitle(name);
    this->show();
}
