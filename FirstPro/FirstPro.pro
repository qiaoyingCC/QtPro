#-------------------------------------------------
#
# Project created by QtCreator 2019-05-03T12:08:43
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = FirstPro
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    mainwindow.cpp

HEADERS  += widget.h \
    mainwindow.h

FORMS    += widget.ui \
    mainwindow.ui
